<!DOCTYPE html>
<html lang="en">
<head>
    <title>Chat On</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="chat.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
 </head>

<body>

<?php
session_start();
if(!isset($_SESSION["username"])) {
    echo "Please enter your name to Join";
    echo "<form method='post' action=''>
    Your Name:<input type='text' name='login_name'>
    <input type='submit' value='submit'>
    </form>";
    if(isset($_REQUEST['login_name'])) {
        $_SESSION['username'] = $_POST["login_name"];
        header('LOCATION:chat.php');
    }
}else{
    echo "<H2>".$_SESSION["username"]."'s Window </H2>";
    echo "<input type='checkbox' id='doReload' align='right' checked> Scroll Lock";

    $fileResource=fopen("chatHistory.txt","r+");
    $read=fread($fileResource, filesize('chatHistory.txt')+1);

    echo "<form action='' method='post' class='window'>
          <textarea id ='show' rows='20' placeholder='No Conversation to show' spellcheck='false' readonly>";

          echo $read;
          fclose($fileResource);

    echo "</textarea>
          <input type='text' name='replyText' class='replyText' placeholder='Type your reply'>
          <input type='submit'  value='submit' class='Reply'>";

    if(isset($_REQUEST['replyText'])) {
        $reply = $_SESSION['username'] . ": " . $_POST['replyText'] . "\n";
        $fileResource=fopen("chatHistory.txt","a+");
        fwrite($fileResource,$reply);
        fclose($fileResource);
    }

    echo "<br>";

    echo "<form action='' method='post'>
        <input type='submit' name='logOut' value='Log Off' class='danger'><br>
        <input type='submit' name='destroyChat' value='Clear History' class='danger'>
    </form>";
    if(isset($_POST['logOut'])){
        session_destroy();
        header('LOCATION:chat.php');
    }
    if(isset($_POST['destroyChat'])){
        $fp=fopen('chatHistory.txt', 'w');
        fwrite($fp,'');
        fclose($fp);
    }
}

?>

<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script>
    $(document).ready(
        function(){
            setInterval(function() {
                if(document.getElementById("doReload").checked) {
                    $("#show").load("chatHistory.txt")
                    var myElement = document.getElementById("chatHistory");
                    myElement.scrollTop = myElement.scrollHeight;
                }
            } ,1000);
        }
    )
    var textarea = document.getElementById('show');
    textarea.scrollTop = textarea.scrollHeight;
</script>


</body>
</html>
