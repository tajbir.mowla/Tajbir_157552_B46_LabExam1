<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Chess Board</title>
    <style>
        body{
            background: lightblue ;
            Height: auto;
            width: auto;
            align-content: center;
        }
        .blackBox{
            margin:0;
            padding: 0%;
            float: left;
            background-color: black;
            height: 50px;
            width: 50px;
        }
        .whiteBox{
            margin:0;
            padding: 0%;
            float: left;
            background-color: white;
            height: 50px;
            width: 50px;
        }
        .chessBoard{
            padding: 5px;
        }
        input[type="text"] {
            width: 20%;
            text-align: left;
            padding: 10px;
            margin: 5px 0;
            border: 0;
            float: inherit;
        }

        input[type="submit"] {
            padding: 5px 20px;
            margin-bottom: 5px;
            color: #1c1c1c;
            text-transform: uppercase;
            background-color: transparent;
            border: 1px solid #1c1c1c;
            outline: 0;
            outline-offset: 0;
            cursor: pointer;
        }
        .box{

        }
    </style>
</head>
<body>
<form action='' method="post">
    <input type="text" name="gridCount" placeholder="Please Enter Grid Number"><br>
    <input type="submit" value="Generate" align="right">
</form>

<?php
function BlackBox($gridSize){
    return "<div class='blackBox'></div>";
}
function WhiteBox($gridSize){
    return "<div class='whiteBox'></div>";
}

$gridSize=50;

if(isset($_REQUEST['gridCount'])) {
    $gridWidth = $_REQUEST['gridCount'] * $gridSize;

    if (isset($_REQUEST['gridCount'])) {
        $gridCount = $_REQUEST['gridCount'];
        //determine size of the chess board
        echo "<div class=chessBoard style=\"height:" . $gridWidth . "px; width:" . $gridWidth . "px\">";
        // looping through each box
        for ($row = 0; $row < $gridCount; $row++) {
            for ($col = 0; $col < $gridCount; $col++) {
                $total = $row + $col;
                if ($total % 2 == 0) {
                    echo WhiteBox($gridSize);
                } else {
                    echo BlackBox($gridSize);
                }
            }
        }
        echo "</div>";
    }
}
?>

</body>
</html>