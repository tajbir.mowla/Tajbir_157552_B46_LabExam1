<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Multiple File Upload Form</title>
    <style>
        body{
            background: lightblue ;
            Height: auto;
            width: auto;
            align-content: center;
            font-family: Calibri, serif;
            font-size: large;
            font-weight: bold;
        }
        input[type='file']{
            width: 100%;
            text-align: left;
            padding: 10px;
            margin: 10px 0;
            float: inherit;
        }
        input[type="submit"] {
            padding: 5px 20px;
            margin-bottom: 5px;
            color: #1c1c1c;
            text-transform: uppercase;
            background-color: transparent;
            border: 1px solid #1c1c1c;
            outline: 0;
            outline-offset: 0;
            cursor: pointer;
        }
        .box{
            height: 50%;
            width: 50%;
            border: groove;
            background-color: lightcyan;
            font-weight: 400;
        }
    </style>
</head>
<body>

<form class="box" action="" method="post" enctype="multipart/form-data">
    File Upload Form.
    <input name="File2Upload[]" type="file" multiple="multiple" ><br />
    <input type="submit" value="Send files" />
</form>

<?php
if(isset($_FILES['File2Upload'])){
    $fileArray=$_FILES['File2Upload'];
    $fileQty = count($fileArray['name']);

    for($i=0;$i<$fileQty;$i++){
        $fileName=$fileArray['name'][$i];
        $tempLocation= $fileArray['tmp_name'][$i];
        $fileDestination = "UploadedFiles/".time().$fileName;
        move_uploaded_file($tempLocation,$fileDestination);

        if($fileArray["error"][$i]==0){
            echo "Your file $fileName has been uploaded successfully. <br>";
        }else{
            echo "Sorry! your file $fileName has not been uploaded <br>";
        }
    }
}
?>


</body>
</html>